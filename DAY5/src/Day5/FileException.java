package Day5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


class FileException {
	
	@SuppressWarnings("unused")
	
	public static void main(String[] args) {
		
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader("C:\\Desktop\\Document.pdf"));
			String line ;
			while((line = bufferedReader.readLine()) != null) {
				System.out.println(line);
			}
			BufferedReader bufferedReader1 = new BufferedReader(new FileReader("C:\\\\Desktop\\\\Document1.pdf"));
			String line2 ;
			while((line2 = bufferedReader1.readLine()) != null) {
				System.out.println(line);
			}
			bufferedReader.close();
			bufferedReader1.close();
			
		}
		catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
		
	}

}
