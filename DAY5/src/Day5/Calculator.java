package Day5;
import java.util.Scanner;


class LessThanZeroException extends Exception{

	private static final long serialVersionUID = 1L;

	public LessThanZeroException(String s) {
		super(s);
	}

}


class MyCalculator {
	public long Power(int num1, int num2) throws LessThanZeroException {
		if(num1 ==0 && num2 ==0) {
			throw new LessThanZeroException("java.lang.Exception : The Number1 and Number2 should not be Zero");
			
		}
		else if(num1<0 || num2<0) {
			throw new LessThanZeroException("java.lang.Exception : The Number1 or Number2 should not be Negative");
		}
		else {
		return (long) Math.pow(num1, num2);
		}
	}

}